import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'landing-page',
      component: require('@/components/pantalla1').default,
      children: [
        {
          path: '/',
          component: require('@/components/categorias').default
        },
        {
          path: '/categoria/:id',
          component: require('@/components/categoria').default
        }
      ]
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
})
