import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  strict: process.env.NODE_ENV !== 'production',
  state: {
    categoriaSeleccionada: '',
    categorias: [
      {id: 1, nombre: "Desayunos"},
      {id: 2, nombre: "Almuerzos"},
      {id: 3, nombre: "Aperitivos"},
      {id: 4, nombre: "Bebidas"},
      {id: 5, nombre: "Coctails"},
    ],
    productos: [
      {id: 1, nombre: "Arepas", precioUnidad: 12.45, descripcion:"Arepas", categoria_id:1},
      {id: 2, nombre: "Huevos", precioUnidad: 12.45, descripcion:"Huevos", categoria_id:1},
      {id: 3, nombre: "Pollo", precioUnidad: 12.45, descripcion:"Pollo", categoria_id:2},
      {id: 4, nombre: "Pasta", precioUnidad: 12.45, descripcion:"Pasta", categoria_id:2},
      {id: 5, nombre: "Ensalada", precioUnidad: 12.45, descripcion:"Ensalada", categoria_id:3},
      {id: 6, nombre: "Galletas", precioUnidad: 12.45, descripcion:"Galletas", categoria_id:3},
      {id: 7, nombre: "Whisky", precioUnidad: 12.45, descripcion:"Whisky", categoria_id:4},
      {id: 8, nombre: "Cerveza", precioUnidad: 12.45, descripcion:"Cerveza", categoria_id:4},
      {id: 9, nombre: "Daikiri", precioUnidad: 12.45, descripcion:"Daikiri", categoria_id:5},
      {id: 10, nombre: "Cuba Libre", precioUnidad: 12.45, descripcion:"Cuba Libre", categoria_id:5},
    ],
    orden: []
  },
  mutations: {
    setCategoriaSeleccionada(state, payload)
    {
      state.categoriaSeleccionada = payload.categoria
    },
    addProductToOrder(state, payload)
    {
      let indice = state.orden.findIndex(elem => elem.id == payload.id);
      if(indice == -1)
      {
        let elem = state.productos.find(elem => elem.id == payload.id);
        state.orden.push(elem);
        let aux_index = state.orden.findIndex(elem => elem.id == payload.id);
        let aux = state.orden.find(elem => elem.id == payload.id);
        aux.unidad = 1;
        aux.total = (parseFloat(aux.precioUnidad) * parseFloat(aux.unidad)).toFixed(2);
        Vue.set(state.orden, aux_index, aux);
      }
      else
      {
        let elem = state.orden[indice];
        elem.unidad++;
        elem.total = (parseFloat(elem.precioUnidad) * parseFloat(elem.unidad)).toFixed(2);
        Vue.set(state.orden, indice, elem);
      }
    },
    uploadField(state, payload)
    {
        let elem = state.orden[payload.index];
        elem[payload.propiedad] = payload.cantidad;
        elem.total = (parseFloat(elem.precioUnidad) * parseFloat(elem.unidad)).toFixed(2);
        Vue.set(state.orden, payload.index, elem);
    }
  }  
})
