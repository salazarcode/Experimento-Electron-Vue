# PUNTO DE VENTA ELECTRON

Esta es una aplicación de puntos de venta. Pensada para dispositivos táctiles, por ahora simplemente ensambla la órden del cliente a partir de una lista de productos de prueba y muestra el resumen de la órden, entiendase: cantidad de productos, subtotal, IVA y total.

#### ESPECIFICACIONES TÉCNICAS

Esta aplicación la creé utilizando la stack web HTML, CSS y Javascript, con el lenguaje preprocesado SASS para los estilos. Webpack para controlar dependencias y compilado. 

Utilicé el framework Javascript Vue.JS para generar las vistas en componentes independientes.

Utilicé el manejador de estado centralizado VUEX y el router de VUE, VUE-ROUTER.

Para llevar la aplicación al desktop utilicé el boilerplate de [Electron-Vue](https://github.com/SimulatedGREG/electron-vue) de código abierto. 

Para el empaquetado instalé Electron-Builder, que también es codigo abierto.

#### USO Y PRUEBA DE ESTA APLICACIÓN ####

Para instalar y ejecutar la aplicación, debes instalar éste runtime:

1. Instala Node, que es el servidor de Javascript.

2. Instala [Yarn](https://yarnpkg.com/en/docs/install#debian-stable) que es el manejador de paquetes.

3. Clona éste repositorio en una carpeta y luego en la linea de comandos, llega hasta ese directorio.

4. Una vez dentro escribe y ejecuta el comando **yarn** para que se instalen las dependencias del proyecto.

5. Ahora simplemente ejecuta **yarn run dev**, con eso se ejecutará la aplicación en desarrollo.

6. Si quieres construir el **.exe** si estas en Windows escribe el comando **yarn build** y voilá!